import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const actions = {
  jia(context, value) {
    // console.log('jia被调用了', context, value);
    context.commit('JIA', value)
  },
  jian(context, value) {
    context.commit('JIAN', value)
  },
  odd(context, value) {
    context.commit ('ODD', value)
  },
  even(context, value) {
    context.commit('EVEN', value)
  },
  addOdd(context, value) {
    context.commit ('ADDODD', value)
  },
  addWait(context, value) {
    context.commit('ADDWAIT', value)
  },
}

const mutations = {
  JIA(store, value) {
    // console.log('JIA被调用了', context, value);
    state.sum += value
  },
  JIAN(store, value) {
    state.sum -= value
  },
  ODD(store, value) {
    state.sum += value
  },
  EVEN(store, value) {
    state.sum *= value
  },
  ADD2(store, value) {
    state.sum += value
  },
  SUB2(store, value) {
    state.sum -= value
  },
  ADD3(store, value) {
    state.sum += value
  },
  SUB3(store, value) {
    state.sum -= value
  },
  ADDODD(store, value) {
    state.sum += value
  },
  ADDWAIT(store, value) {
    state.sum *= value
  },

  // PERSON组件
  ADD_PERSON(store, value) {
    state.personList.unshift(value) 
  }
}

const state = {
  sum: 1,
  location: 'beijing',
  subject: '前端',
  personList: [
    {name: 'zhangsan'},
    {name: 'lisi'},
    {name: 'wangwu'},
  ]
}

const getters = {
  bigSum(state) {
    return state.sum * 10
  }
}

export default new Vuex.Store({
  actions,
  mutations,
  state,
  getters,
})