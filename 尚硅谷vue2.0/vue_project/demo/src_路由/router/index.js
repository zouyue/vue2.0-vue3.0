import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import About from '../components/About'
import Home from '../components/Home'
export default new VueRouter({
  routes: [
    {
      path: '/about',
      component: About
    },
    {
      path: '/home',
      component: Home
    }
  ]
})