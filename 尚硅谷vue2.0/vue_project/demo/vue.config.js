module.exports = {
  // devServer: {
  //   proxy: 'http://localhost:5000'
  // },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:5000',
        pathRewrite: {'^/api': ''},
        ws: true,
        changeOrigin: true
      },
      '/foo': {
        target: 'http://localhost:5001',
        pathRewrite: {'^/foo': ''},
      }
    }
  }
}