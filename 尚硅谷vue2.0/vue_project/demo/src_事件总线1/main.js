import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// 事件总线
// 方式2
// const Demo = Vue.extend({})
// const d = new Demo()
// Vue.prototype.x = d

// 方式1
// window.x = {a: 10, b: 20}

new Vue({
  render: h => h(App),
  // 方式3：标准写法
  beforeCreate() {
    // 全局事件总线
    Vue.prototype.$bus = this
  }
}).$mount('#app')
